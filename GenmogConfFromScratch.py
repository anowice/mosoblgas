#!/usr/bin/env python

import json

modbus_config = {}
entry = {}
entry['SLAVE_ADDR'] = 16
entry['START_ADDR'] = 0
entry['COUNT'] = 46
entry['INTERFACE'] = 'ttyM0'  # /dev/ should be added to this string
entry['FUNCTION'] = 3           # only 3 and 4 supported
entry['MODBUS_CHECK_INTERVAL'] = 10   # repeat interval for this request only
modbus_config['1'] = entry      # name of the modbus request, used as a key

entry = {}
entry['SLAVE_ADDR'] = 1
entry['START_ADDR'] = 2
entry['COUNT'] = 10
entry['INTERFACE'] = 'ttyM0'
entry['FUNCTION'] = 3
entry['MODBUS_CHECK_INTERVAL'] = 10
#modbus_config['2'] = entry

interfaces_config = {}
entry = {}
entry['SPEED'] = 9600
entry['BYTE_SIZE'] = 8
entry['PARITY'] = 'None'
entry['STOP_BITS'] = 1
entry['TIMEOUT'] = 1
entry['MODE'] = 'rtu'
interfaces_config['ttyM0'] = entry
interfaces_config['ttyM1'] = entry

metrics_config = {}
entry = {}
entry['DATA_TYPE'] = 'float'
entry['MB_REQUEST'] = '1'      # name of the modbus request
entry['SWAP_WORD'] = True    # modbus word order for float values
entry['REG_ADDR'] = 4
entry['DELTA'] = 0.2          # minimal significant change of the value
entry['STATUS_MV110'] = 'Sensor | Outdoor Air | Temperature | Status'
metrics_config['Sensor | Outdoor Air | Temperature | Value'] = entry
entry = {}
entry['DATA_TYPE'] = 'float'
entry['MB_REQUEST'] = '1'      # name of the modbus request
entry['SWAP_WORD'] = True    # modbus word order for float values
entry['REG_ADDR'] = 10
entry['DELTA'] = 0.5
entry['STATUS_MV110'] = 'Sensor | Return Water | Temperature | Status'
metrics_config['Sensor | Return Water | Temperature | Value'] = entry
entry = {}
entry['DATA_TYPE'] = 'float'
entry['MB_REQUEST'] = '1'      # name of the modbus request
entry['SWAP_WORD'] = True    # modbus word order for float values
entry['REG_ADDR'] = 16
entry['DELTA'] = 0.025
entry['STATUS_MV110'] = 'Sensor | Return Water | Pressure | Status'
metrics_config['Sensor | Return Water | Pressure | Value'] = entry


entry = {}
entry['DATA_TYPE'] = 'word'
entry['MB_REQUEST'] = '1'
entry['REG_ADDR'] = 2
metrics_config['Sensor | Outdoor Air | Temperature | Status'] = entry
entry = {}
entry['DATA_TYPE'] = 'word'
entry['MB_REQUEST'] = '1'
entry['REG_ADDR'] = 8
metrics_config['Sensor | Return Water | Temperature | Status'] = entry
entry = {}
entry['DATA_TYPE'] = 'word'
entry['MB_REQUEST'] = '1'
entry['REG_ADDR'] = 14
metrics_config['Sensor | Return Water | Pressure | Status'] = entry
entry = {}
entry['DATA_TYPE'] = 'word'
entry['MB_REQUEST'] = '1'
entry['REG_ADDR'] = 19
metrics_config['Sensor | Fire Alarm | Status'] = entry
entry = {}
entry['DATA_TYPE'] = 'word'
entry['MB_REQUEST'] = '1'
entry['REG_ADDR'] = 25
metrics_config['Sensor | Gas shutoff value | Status'] = entry
entry = {}
entry['DATA_TYPE'] = 'diag'
entry['DIAG'] = 'DIAG1'
metrics_config['Sensor | Link BB modbus RTU | Status'] = entry

HTTP_config = {}
HTTP_config['SERVER'] = '192.168.135.5'
HTTP_config['AGENT_ID'] = '4954811657509174781'
HTTP_config['SEND_URL'] = 'https://79.143.70.190:9443/api/nbi/telemetry'
HTTP_config['SEND_INTERVAL'] = 30
HTTP_config['API_KEY'] = 'RE499kcKJ1FCDPP158kaS7M2'


full_config = {}
full_config['INTERFACES'] = interfaces_config
full_config['MODBUS'] = modbus_config
full_config['METRICS'] = metrics_config
full_config['HTTP'] = HTTP_config

with open('mogconfig.json', mode='w') as f:
    json.dump(full_config, f, indent=4, separators=(',', ': '), encoding='utf8', sort_keys=True, )
print('Config is written')
