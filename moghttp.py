#!/usr/bin/env python

# Python modules
import os
import json
import sys
import time
import logging
logging.basicConfig(stream=sys.stdout, format="%(asctime)s %(message)s", level=logging.DEBUG)
import datetime
import Queue
import threading
import random
import urllib2
import ssl
import socket
from collections import defaultdict
# Third-party modules
import mogmodbus


# Settings
AGENT_ID = int(os.environ.get("AGENT_ID","4954811657509174781"))
MODBUS_DEV = os.environ.get("MODBUS_DEV", "/dev/ttyUSB0")
MODBUS_SLAVE = int(os.environ.get("MODBUS_SLAVE", "16"))
MODBUS_CHECK_INTERVAL = int(os.environ.get("MODBUS_CHECK_INTERVAL", 5))
#SEND_URL = os.environ.get("SEND_URL", "https://192.168.130.132/api/nbi/telemetry")
SEND_URL = os.environ.get("SEND_URL", "https://79.143.70.190:9443/api/nbi/telemetry")
SEND_INTERVAL = int(os.environ.get("SEND_INTERVAL", 30))
API_KEY = os.environ.get("API_KEY", "RE499kcKJ1FCDPP158kaS7M2")

HEADERS = {
    "Private-Token": API_KEY
}
SSL_CTX = ssl._create_unverified_context()


def die(msg):
    logging.error(msg)
    sys.exit(1)

def setup_logging():
    logging.basicConfig(stream=sys.stdout, format="%(asctime)s %(message)s", level=logging.DEBUG)

def main():
    setup_logging()
    q = Queue.Queue()

    conf = mogmodbus.MOGConfig('/home/moxa/mogconfig.json')
    mb = mogmodbus.Modbus(conf.iface, conf.metrics, conf.mb)
    for interface in conf.iface:
        iface_thread = threading.Thread(target=mb.reader, args=(interface,))
        iface_thread.setDaemon(True)
        iface_thread.start()
        logging.info('Started reading modbus using ' + interface)

    #metrics_thread = threading.Thread(target=mb.metric_read, args=(q,))
    metrics_thread = threading.Thread(target=mb.metric_changed, args=(q,))
    metrics_thread.setDaemon(True)
    metrics_thread.start()
    logging.info('Started checking metrics')

    # Prevent service hammering
    time.sleep(random.randint(0, SEND_INTERVAL))
    #
    data = defaultdict(list)  # metric_type, path -> [measurements, ...]
    while True:
        # Read collected measurements
        while True:
            try:
                metric_type, value, ts = q.get_nowait()
                metric_splited = metric_type.split(' | ')
                path = str(metric_splited[1])
                del metric_splited[1]
                metric_type = ' | '.join(metric_splited)
                data[metric_type, path] += [(ts, value)]
            except Queue.Empty:
                break  # No new data
        if data != {}:
            # Send data
            msg = {
                "bi_id": AGENT_ID,
                "metrics": []
            }
            for k in data:
                metric_type, path = k
                msg["metrics"] += [{
                    "metric_type": metric_type,
                    "path": [path],
                    "values": data[k]
                }]
            # Send
            logging.info("Sending POST %s: %s", SEND_URL, msg)
            with open('/home/moxa/http.log', mode='a') as f:
                f.write(str(msg))
                f.write('\n\n')

            req = urllib2.Request(SEND_URL, data=json.dumps(msg), headers=HEADERS)
            try:
                f = urllib2.urlopen(req, timeout=10, context=SSL_CTX)
                logging.info("Response: %s", f.read())
                # Reset collected data
                data = defaultdict(list)
            except (urllib2.URLError, socket.error) as e:
                logging.error("Error: %s", e)
            # Wait
            #time.sleep(SEND_INTERVAL)
            time.sleep(0.3)


if __name__ == "__main__":
    if not AGENT_ID:
        die("AGENT_ID is not set")
    main()
