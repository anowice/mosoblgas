#!/usr/bin/env python

# Python modules
from __future__ import print_function
import json
import pprint
import sys
import logging
logging.basicConfig(stream=sys.stdout, format="%(asctime)s %(message)s", level=logging.DEBUG)
import threading
import struct
import time
import Queue
import datetime
import os
# Third-party modules
import minimalmodbus
import serial

# max 5 min beeing offline
MAX_UNCHANGED_TIME = 4.5*60
parity_serial_const = {'None': serial.PARITY_NONE, 'Odd': serial.PARITY_ODD,
                       'Even': serial.PARITY_EVEN}

class MOGConfig(object):
    def __init__(self, file_name):
        """Reads configuration from JSON file"""
        with open(file_name, mode='r') as f:
            config = json.load(f, encoding='utf-8')
        try:
            self.mb = config['MODBUS']
            self.iface = config['INTERFACES']
            self.metrics = config['METRICS']
            self.http = config['HTTP']
            logging.info('Configuration loaded...')
        except KeyError:
            logging.error('Error reading JSON configuration file for MosOblGas controller. Exiting...')
            sys.exit(1)


class Modbus(object):
    def __init__(self, iface, metrics, mb, metric_check_interval=1.0, sheduler_tick=0.5):
        """ iface,metric,mb - dicts containing interface, metric and modbus related configurations respectively
        metric_check_interval - time to sleep in a metric cheking thread
        sheduler_tick - time to sleep in a thread of modbus request scheduler
        """
        self.metric_check_interval = metric_check_interval
        self.iface = iface
        self.metrics = metrics
        self.mb = mb
        self.mb_values = {}
        self.mb_values_lock = threading.Lock()

        self.mb_timers = dict.fromkeys(self.mb.keys())
        for key in self.mb_timers:
            self.mb_timers[key] = float(self.mb[key]['MODBUS_CHECK_INTERVAL'])

        self.mb_timeon = dict.fromkeys(self.mb.keys())
        for key in self.mb_timeon:
            self.mb_timeon[key] = True
        self.mb_timeon_lock = threading.Lock()
        self.tick = sheduler_tick

        self.metric_values = {}
        self.metric_values_old = {}

        self.instruments = {}
        self._set_instruments()
        self.check_cnt = 0
        scheduler_thread = threading.Thread(target=self.reqest_scheduler)
        scheduler_thread.setDaemon(True)
        scheduler_thread.start()
        logging.info('Started Modbus scheduler.')

    def reqest_scheduler(self):
        """reqest_scheduler calcs timers for every modbus request individualy. Stores TON flags in mb_timeron for
        use in reader routine.
        """
        while True:
            time.sleep(self.tick)
            for key, req in self.mb.items():
                self.mb_timers[key] -= self.tick
                if self.mb_timers[key] < 0.0:
                    self.mb_timers[key] = float(req['MODBUS_CHECK_INTERVAL'])
                    with self.mb_timeon_lock:
                        self.mb_timeon[key] = True



    def _set_instruments(self):
        """ Setting up minimalmodbus Instruments for every Modbus request """
        for key, req in self.mb.items():
            # Interface and Modbus Slave Addres as a dict key
            ik = (req['INTERFACE'], req['SLAVE_ADDR'])
            iface = self.iface[req['INTERFACE']]
            if ik not in self.instruments:
                try:
                    self.instruments[ik] = minimalmodbus.Instrument('/dev/' + req['INTERFACE'],
                                                                    req['SLAVE_ADDR'])
                except serial.serialutil.SerialException:
                    logging.exception('No such serial device: ' + req['INTERFACE'])
                    sys.exit(1)
            self.instruments[ik].serial.baudrate = iface['SPEED']
            self.instruments[ik].serial.parity = parity_serial_const[iface['PARITY']]
            self.instruments[ik].serial.bytesize = iface['BYTE_SIZE']
            self.instruments[ik].serial.stopbits = iface['STOP_BITS']
            self.instruments[ik].serial.timeout = iface['TIMEOUT']
            self.instruments[ik].serial.mode = iface['MODE']
            logging.debug("Minimalmodbus used settings")
            logging.debug(self.instruments)


    def reader(self, iface):
        """Executes all Modbus requests for particular interface.
        Should be started as separate threads for every interface.
        """
        while True:
            for key, req in self.mb.items():
                if req['INTERFACE'] == iface and self.mb_timeon[key]:
                    with self.mb_timeon_lock:
                        self.mb_timeon[key] = False
                    mb_result = {}
                    mb_result['Status'] = 'Failure'
                    mb_result['ts'] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
                    start_mb_addr = req['START_ADDR']
                    mb_count = req['COUNT']
                    end_mb_addr = req['START_ADDR'] + req['COUNT']
                    ik = (req['INTERFACE'], req['SLAVE_ADDR'])
                    try:
                        values = self.instruments[ik].read_registers(start_mb_addr, mb_count, req['FUNCTION'])
                        index = 0
                        for mb_addr in range(start_mb_addr, end_mb_addr):
                            # Use Modbus register address as a dict key
                            mb_result[mb_addr] = values[index]
                            index += 1
                        mb_result['ts'] = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
                        mb_result['Status'] = 'Success'
                        logging.debug('Modbus request N {0} successful'.format(key))
                        logging.debug(mb_result)
                    except IOError:
                        logging.error("Error reading device. Modbus request: {0}".format(key))
                    except ValueError:
                        logging.error("Slave device reported an error. Modbus request {0}".format(key))
                    with self.mb_values_lock:
                        self.mb_values[key] = mb_result
            time.sleep(self.tick)

    def _get_diag_value(self, diag):
        if diag == 'DIAG1':
            modbus_ok = -2                      # -1 means True (OK)
            for key in self.mb_values:
                # Unseccessful response exists
                if self.mb_values[key]['Status'] == 'Failure':
                    modbus_ok = 2               # 1 meaans False (Not OK)
            ts = self.mb_values[key]['ts']
        return (modbus_ok,ts)

    def _get_word_value(self, req, addr):
        try:
            # SOVA database cann't store zero values, so this wierd thing created:
            actual_value = self.mb_values[req][addr]
            if actual_value == 0:
                actual_value = -1
            return (actual_value, self.mb_values[req]['ts'])
        except:
            logging.error("{0} register has no corresponding space in the receiving buffer.".format(addr))
            sys.exit(1)

    def _get_float_value(self, req, addr, swap_words=False):
        try:
            w1 = self.mb_values[req][addr]
            w2 = self.mb_values[req][addr + 1]
        except KeyError:
            logging.error("{0} register has no corresponding space in the receiving buffer.".format(addr))
            sys.exit(1)
        if swap_words:
            w1, w2 = w2, w1
        t = (w1, w2)
        ps = struct.pack('HH', *t)
        fv = struct.unpack('f', ps)[0]
        return ('{:.4}'.format(fv), self.mb_values[req]['ts'])

    def _calc_metrics(self):
        """ Creates metric, value, ts from read modbus registers, and as special case diagnostic tags """
        self.metric_values = {}
        with self.mb_values_lock:
            for key, metric in self.metrics.items():
                # Calculation of diag metric when modbus request completed
                if metric['DATA_TYPE'] == 'diag':
                    if self.mb_values != {}:
                        self.metric_values[key] = self._get_diag_value(metric['DIAG'])
                else:
                    # Modbus responce exist
                    if metric['MB_REQUEST'] in self.mb_values:
                        # Modbus respo[Ance successful
                        if self.mb_values[metric['MB_REQUEST']]['Status'] == 'Success' :
                            if metric['DATA_TYPE'] == 'word':
                                self.metric_values[key] = self._get_word_value(metric['MB_REQUEST'], metric['REG_ADDR'])
                            elif metric['DATA_TYPE'] == 'float':
                                self.metric_values[key] = self._get_float_value(metric['MB_REQUEST'], metric['REG_ADDR'],
                                                                                metric['SWAP_WORD'])
            self.mb_values = {}
        self._check_status()

    def _check_status(self):
        """ Delete metric values from dict in case of error status related to that metric.
        This routine is hardware specific to OWEN MV110 input module """
        keys = list(self.metric_values)
        for key in keys:
            related_status = self.metrics[key].get('STATUS_MV110','')
            if related_status != '':
                if self.metric_values[related_status][0] != -1:
                    self.metric_values.pop(key)

    def metric_changed(self, queue):
        """Will put all metrics in a queue if one or more values has changed since last check.
        Should be started as a thread.
        """
        while True:
            time.sleep(self.metric_check_interval)
            self.check_cnt += self.metric_check_interval
            self._calc_metrics()
            changed = False
            for key, metric in self.metrics.items():
                if key in self.metric_values:
                    val = self.metric_values[key][0]
                    ts  = self.metric_values[key][1]
                    if key not in self.metric_values_old:
                        changed = True
                        logging.debug('Brand new metric: {0}: {1}'.format(key,val))
                    else:
                        val_old = self.metric_values_old[key][0]
                        logging.debug(str(key)+str(val_old))
                        if metric['DATA_TYPE'] == 'word' and val != val_old:
                            changed = True
                            logging.debug('Uint metric renewal: {0}: {1}'.format(key,val))
                        elif metric['DATA_TYPE'] == 'float' and abs(float(val) - float(val_old)) > metric['DELTA']:
                            changed = True
                            logging.debug('Float metric renewal: {0}: {1}'.format(key,val))
                        else:
                            pass
                            logging.debug('{0} hasn\'t changed...'.format(key, val))
                    if self.check_cnt > MAX_UNCHANGED_TIME:
                        changed = True
            if changed:
                self.check_cnt = 0.0
                self.metric_values_old.update(self.metric_values)
                for key, metric in self.metrics.items():
                    if key in self.metric_values:
                        val = self.metric_values[key][0]
                        ts  = self.metric_values[key][1]
                        queue.put((key, val, ts))
                        logging.debug('Sending metric: {0}: {1} : {2}'.format(key, val, ts))

    def metric_read(self, queue):
        """Will put (metric,value,timestamp) in a queue if value is read from modbus.
        Should be started as a thread.
        """
        while True:
            time.sleep(self.metric_check_interval)
            self._calc_metrics()
            for key, metric in self.metrics.items():
                if key in self.metric_values:
                    val = self.metric_values[key][0]
                    ts  = self.metric_values[key][1]
                    queue.put((key, val, ts))
                    logging.debug('Read metric: {0}: {1} : {2}'.format(key, val, ts))


if __name__ == "__main__":
    conf = MOGConfig('mogconfig.json')
    print("\nInterface config")
    print("-" * 80)
    pprint.pprint(conf.iface)
    print("\nModbus config")
    print("-" * 80)
    pprint.pprint(conf.mb)
    print("\nMetrics config")
    print("-" * 80)
    pprint.pprint(conf.metrics)
    print("\nHTTP config")
    print("-" * 80)
    pprint.pprint(conf.http)
    print("-" * 80)

    mb = Modbus(conf.iface, conf.metrics, conf.mb)
    q = Queue.Queue()

    for interface in conf.iface:
        iface_thread = threading.Thread(target=mb.reader, args=(interface,))
        iface_thread.setDaemon(True)
        iface_thread.start()
        logging.info('Started reading ' + interface)

    q = Queue.Queue()
    #metrics_thread = threading.Thread(target=mb.metric_read, args=(q,))
    metrics_thread = threading.Thread(target=mb.metric_changed, args=(q,))
    metrics_thread.setDaemon(True)
    metrics_thread.start()
    logging.info('Started checking metrics')

    # Print out queue every second for test purposes
    while True:
        try:
            item = q.get_nowait()
            print('--------------------------')
            print(item)
        except Queue.Empty:
            time.sleep(1)
            continue

